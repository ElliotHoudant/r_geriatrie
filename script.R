# ----------------------------------------------------
#
# TP Gériatrie
#
# Antoine Lamer
#
# 01/01/2023
#
# ----------------------------------------------------

# Packages
library(RPostgres)

# Paramètres de connexion
source('connexion.R')

# Connexion à la base de données
con <- dbConnect(RPostgres::Postgres(),dbname = database, 
                 host = host,
                 port = port, # or any other port specified by your DBA
                 user = user,
                 password = password)

# Liste des tables disponibles
dbListTables(con)

# Chargement des dataframes
patient = dbReadTable(con, "patient")
sejour = dbReadTable(con, "sejour")
rum = dbReadTable(con, "rum")
rum_acte = dbReadTable(con, "rum_acte")
rum_diagnostic = dbReadTable(con, "rum_diagnostic")

# Dans la table rum, ajouter un indice correspondant au rang du RUM dans le séjour

# Pour chaque rum, ajouter un rum fictif de rang 0 avec le mode d'entrée du premier RUM

# Pour chaque rum, ajouter un rum fictif de rang r + 1 
# (r étant le rang max de chacun des séjour)
# avec le mode sortie

# Représenter sous forme graphique l'effectif de chacun des modes d'entrée

# Représenter sous forme graphique l'effectif de chacune des UFs (pour tous les RUMs)

# Représenter sous forme graphique l'effectif des UFs des premiers RUMs

# Représenter sous forme graphique l'effectif de chacun des modes de sortie

# Compter les associations d'UFs pour les deux premiers RUMs.

# Compter les associations d'UFs pour toutes les transitions entre RUMs.

# Compter les transitions du mode d'entrée vers le premier RUM.

# Compter les transitions du dernier rum vers le mode de sortie.

# Compter les séquences complètes (ex : Urgences - UF1 - UF2 - Domicile = 5 séjours).